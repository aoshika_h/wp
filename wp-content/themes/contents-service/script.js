(function($){
	/*var wipe = [
		"perspectiveLeft",
		"perspectiveRight",
		"slideLeft",
		"slideRight",
		"openDownLeftOut",
		"openDownRightOut",
		"openUpLeftOut",
		"openUpRightOut"
	];*/
	var wipe = [
		"slideLeft"
	];
	var contents = $( "li" );
	var index = contents.length - 1;
	var zindex = 0;
	var randnum = 0;
	var action_class = "";

	(function loop(){

		get_emerge();

		//randnum = Math.floor( Math.random() * wipe.length );
		var c = contents.eq( index );
		var duration = c.attr( "duration" );

		var action = function(){
			action_class = wipe[ randnum ];
			c.addClass( action_class );

			setTimeout( function(){
				c.css( "zIndex", --zindex ).removeClass( action_class );
				if( index - 1 >= 0 ){
					index--;
				}else{
					index = contents.length - 1;
				}
				loop();
			}, 1000 );
		};

		if( c.hasClass( "movie" ) ){
			v = c.find("video");
			v[0].currentTime = 0;
			v[0].play();
			var v_action = function(){
				v[0].pause();
				action();
				v[0].removeEventListener( "ended", v_action, false );
			};
			v[0].addEventListener( "ended", v_action, false );
		}else{
			setTimeout( action , duration * 1000 );
		}

	})();


})($);

//緊急情報取得
function get_emerge (){

	var get_data = function( $data ){

		//表示記事に変更があった場合更新。
		if(
			( newest_post_date && $data.current_newest_post ) &&
			( newest_post_date < $data.current_newest_post )
		){
			location.reload();
		}

		if( $data.post_status != "null" ){
			if( $( "#post_title" ).html() == "" ){
				$( "#post_title" ).html( $data.post_title );
				$( "#post_content" ).html( $data.post_content );
				$( "#emerge" ).width( $( "#emerge" ).width() ).removeClass( "puffOut" ).addClass( "puffIn" );
				setTimeout( marquee, 3000 );
			}
		}else{
			$( "#emerge" ).removeClass( "puffIn" );
			if( $( "#post_title" ).html() ) $( "#emerge" ).addClass( "puffOut" );
			setTimeout( function(){ $( "#post_title, #post_content" ).empty(); }, 1000 );
		}
	};
	$.getJSON( "http://hospital.aquacp.com/?get_emerg=1", "", get_data );

}

//緊急情報をマーキーする
function marquee (){
	var h = $("#post_content h1, #post_content h2, #post_content h3, #post_content h4, #post_content h5, #post_content h6, #post_content p");
	var cnt = 0;
	var text_ary = [];

	for( var i = 0, n = h.length; i < n; i++ ){
		h.eq(i).width( h.eq(i).width() );
	}

	var action = function( obj, num ){
		this.obj = obj;
		this.num = num;
	};
	action.prototype.loop = function(){
		var _this = this;
		_this.obj.delay( _this.num * 1000 ).animate({ marginLeft: -1 * $( "#emerge" ).width() - 100 }, { easing: "linear", duration: 10000, complete: function(){
			_this.obj.css( "marginLeft", $( "#emerge" ).width() + 100 );
			_this.loop();
		} } );
	};

	for( var i = 0, n = h.length; i < n; i++ ){
		text_ary[ i ] = new action( h.eq(i), i );
		text_ary[ i ].loop();
	}

}



