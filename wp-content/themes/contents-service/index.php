<?php

//最新の投稿チェック
$args = array(
	"category" => -6,
	'posts_per_page' => 1
);

$check_post = get_posts( $args );
$select_posts = get_field( "select_post" );

//緊急情報取得
if( $_GET["get_emerg"] ):


	//緊急情報を取得
	$args = array(
		"category" => 6,
		'posts_per_page' => 1
	);
	$post = get_posts( $args );
	$post_time = mysql2date( "U", $post[0]->post_date );
	$current_time = date("U");

	//現在の一番新しい投稿を取得
	$args = array(
		"category" => -6,
		'posts_per_page' => 1
	);
	$current_check_post = get_posts( $args );

	foreach( $select_posts as $p ):
		if( $p->ID == $current_check_post[0]->ID ):
			$post[0]->current_newest_post = mysql2date( "U", $current_check_post[0]->post_date );
			break;
		else:
			$post[0]->current_newest_post = "";
		endif;
	endforeach;


	//緊急情報が公開されていればブラウザに渡す
	if( $post[0]->post_status == "publish" ):
		$post[0]->post_content = apply_filters( "the_content", $post[0]->post_content );
	else:
		$post[0]->post_status = "null";
	endif;

	echo json_encode( $post[0] );
	exit;
endif;


?>
<!DOCTYPE html>
<html>
<head>
	<title>病院コンテンツ配信プロトタイプ</title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/style.css" type="text/css" media="screen" charset="utf-8"/>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/magic.css" type="text/css" media="screen" charset="utf-8"/>
	<script>
		var newest_post_date = "<?php echo mysql2date( "U", $check_post[0]->post_date );?>";
	</script>
</head>
<body>
	<main>
		<ul>
		<?php

			$select_posts = array_reverse( $select_posts );
			foreach( $select_posts as $post ):
				setup_postdata( $GLOBAL["post"] &= $post );
				$cate = get_the_category();
				$duration = get_field( "duration" );
		?>
			<li class="<?php echo $cate[0]->slug;?> magictime" duration="<?php echo empty( $duration )? 10: $duration;?>">
		<?php
				switch( $cate[0]->slug ):

					//フリーコンテンツ
					case "free":{
						echo apply_filters( "the_content", $post->post_content );
						break;
					}

					//画像
					case "image":{
						$image = get_field( "image" );
		?>
				<img src="<?php echo $image["url"];?>" alt="<?php echo $post->post_title; ;?>" />
		<?php
						break;
					}

					//動画
					case "movie":{
						$mov = get_field( "movie" );
		?>
				<video src="<?php echo $mov;?>" height="1080"></video>
		<?php
						break;
					}

					//天気
					case "weather":{
						echo "天気情報を表示させるよ！";
						break;
					}

					//ニュースAタイプ
					case "news-a":{
						echo "ニュースAタイプだよ！";
						break;
					}

					//ニュースBタイプ
					case "news-b":{
						echo "ニュースBタイプだよ！";
						break;
					}

				endswitch;
		?>
			</li>
		<?php
			endforeach;
		?>
		</ul>
		<div id="emerge" class="magictime">
			<div id="post_title"></div>
			<div id="post_content"></div>
		</div>
	</main>
<script src="<?php echo get_stylesheet_directory_uri();?>/jquery-2.1.4.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo get_stylesheet_directory_uri();?>/script.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>