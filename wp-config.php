<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'delivery_system');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'root');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'aquapass123!');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '98-_M;>FJ)U*}@qIX3j9=? XCtRo0RS5|b}o{ }cobRqvBpg%i@sstnuia/Xk~M#');
define('SECURE_AUTH_KEY',  '#fV8E)|i|bjn([q;%5oP4.Fr|[VeJ_OY $!0[)z~wS>XFhWFC+PXN8=meiB#=P%n');
define('LOGGED_IN_KEY',    'F#TK-Xo8rkfX|SwHc2;+bFnih(W,2^|B8@<pU[$N!*&G-44(KP}f=z~U%,hGkU8^');
define('NONCE_KEY',        'C+V`u9(A&F>3+n-=5}KR{AtKIY7WB#I$C|!A{{Orbe^ifNLK>p}Z9%uss3a9+w!%');
define('AUTH_SALT',        't5[(Xe>KE;1NmS#Pj^Nd20Q:.nN|-ZW}}`d/-O*n3iY##Wq<`gzIX<FIc->h/bNu');
define('SECURE_AUTH_SALT', '!H^7H||Nvcnv*x#7+SgMB6r0Syvi/+5{0|bI,u-(0dN}j%CNqDF{^!/)?[xa.c3Y');
define('LOGGED_IN_SALT',   'UF<[N[yI1o&xZ4{G^ad<n<]12:|-s/RP30yC;5%8z%Za#,I&6?+N=#-FZdA-8#|v');
define('NONCE_SALT',       'd`hu|8sPF:_szcvT/BsPtD&Qqn7Fp)nF94Q/<}V-s!W<@i+9P=}|N:~OH<<[I%|^');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'delivery_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
